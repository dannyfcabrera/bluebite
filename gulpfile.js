var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require("gulp-rename"),
    browserSync = require('browser-sync').create(),
    sourcemaps = require('gulp-sourcemaps'),
    source = require('vinyl-source-stream'),
    watchify = require('watchify'),
    browserify = require('browserify'),
    buffer = require('vinyl-buffer'),
    svgmin = require('gulp-svgmin'),
    svgstore = require('gulp-svgstore'),
    inject = require('gulp-inject'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    path = require('path'),
    notify = require('gulp-notify'),
    imagemin = require('gulp-imagemin'),
    assign = require('lodash.assign'),
    cssnano = require('cssnano'),
    ngannotate = require('browserify-ngannotate');


// Files
var scripts = './client/app/**/*.js',
    styles = './client/content/sass/**/*.scss',
    img = './client/content/img/uncompressed/**/*.*',
    vector = './client/content/img/svg/**/*.svg',
    html = './client/content/**/*.html';


/* /////////////
  Gulp Tasks
*///////////////

var customOpts = {
  entries: ['./client/app/app.module.js'],
  debug: true,
  paths: ['./node_modules/','./client/app/']
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts));

b.transform(ngannotate);

gulp.task('js', ['lint'], bundle);
b.on('update', bundle);
b.on('log', gutil.log);

function bundle() {
  return b.bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify({mangle: false})).on('error', gutil.log)
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('./client/dist'));
}

gulp.task('lint', function() {
  return gulp.src(scripts)
    .pipe(jshint())
    .pipe(notify(function (file) {
      if (file.jshint.success) {
        return false;
      }

      var errors = file.jshint.results.map(function (data) {
        if (data.error) {
          return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
        }
      }).join("\n");
      return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
    }));
});

gulp.task('sass', function () {

  var processors = [
    autoprefixer({ browsers: ['last 2 versions'] }),
    cssnano({discardComments: {removeAll: true}})
  ];
  return gulp.src(styles)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(processors))
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('./client/dist'))
    .pipe(browserSync.stream());
});

gulp.task('img', function () {
	return gulp.src(img)
		.pipe(imagemin())
		.pipe(gulp.dest('./client/dist/img'))
    .pipe(browserSync.stream());;
});

gulp.task('svg', function () {
  var svgs = gulp
    .src(vector, { base: './client/content/img/svg' })
    .pipe(svgmin(function getOptions (file) {
      var prefix = path.basename(file.relative, path.extname(file.relative));
      return {
        plugins: [{
          removeStyleElement: true,
          cleanupIDs: {
            prefix: prefix + '-',
            minify: true
          }
        }]
      }
    }))
    .pipe(rename(function (path) {
        var name = path.dirname.split(path.sep);
        name.push(path.basename);
        path.basename = name.join('-');
    }))
    .pipe(svgstore({ inlineSvg: true }));

  function fileContents (filePath, file) {
    return file.contents.toString();
  }

  return gulp
    .src('./client/index.html')
    .pipe(inject(svgs, { transform: fileContents }))
    .pipe(gulp.dest('./client'))
    .pipe(browserSync.stream());
});

gulp.task('js-watch', ['js'], browserSync.reload);


gulp.task('serve', function () {

  browserSync.init({
    server: {
      baseDir: './client',
      routes: {
        "/node_modules": "node_modules"
      }
    }
  });

  gulp.watch(scripts, ['js-watch']);
  gulp.watch(styles, ['sass']);
  gulp.watch(img, ['img']);
  gulp.watch(vector, ['svg']);
  gulp.watch(html).on('change', browserSync.reload);

});


gulp.task('default', ['lint', 'sass', 'svg', 'img', 'js-watch', 'serve'], function () {

});
