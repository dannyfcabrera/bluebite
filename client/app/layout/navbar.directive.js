require('./layout.module');

module.exports = (function() {
  'use strict';

  angular
    .module('game.layout')
    .directive('bbNavbar', bbNavbar);

  function bbNavbar() {
    return {
      templateUrl: 'app/layout/navbar.html',
      restrict: 'E',
      replace: true,
      scope: true,
      controller: NavbarController,
      controllerAs: 'vm'
    };
  }

  NavbarController.$inject = ['$location'];

  function NavbarController($location) {
    var vm = this;

  }

})();
