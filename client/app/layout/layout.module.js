module.exports = (function() {
  'use strict';
  angular.module('game.layout', []);
  require('./navbar.directive');
  require('./header.directive');
  require('./footer.directive');
})();
