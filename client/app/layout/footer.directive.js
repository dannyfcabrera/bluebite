require('./layout.module');

module.exports = (function() {
  'use strict';

  angular
    .module('game.layout')
    .directive('bbFooter', bbFooter);

  function bbFooter() {
    return {
      templateUrl: 'app/layout/footer.html',
      restrict: 'E',
      replace: true,
      scope: {},
      controller: FooterController,
      controllerAs: 'vm'
    };
  }

  function FooterController() {
    var vm = this;

    vm.date = date;

    function date () {
      return new Date();
    }

  }

})();
