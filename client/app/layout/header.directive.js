require('./layout.module');

module.exports = (function() {
  'use strict';

  angular
    .module('game.layout')
    .directive('bbHeader', bbHeader);

  function bbHeader() {
    return {
      templateUrl: 'app/layout/header.html',
      restrict: 'E',
      replace: true,
      scope: true,
      controller: HeaderController,
      controllerAs: 'vm'
    };
  }

  function HeaderController() {
    var vm = this;


  }

})();
