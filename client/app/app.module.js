var $ = require('jquery');
require('angular');
require('angular-route');

require('./core/core.module');
require('./layout/layout.module');
require('./main/main.module');
require('./console/console.module');


(function() {
  'use strict';

  angular
      .module('blueBiteGame',
      [
        'ngRoute',
        'game.core',
        'game.layout',
        'game.main',
        'game.console'
      ])
      .config(config)
      .run(run);

  function config($routeProvider) {
      $routeProvider
      .otherwise({
        redirectTo: '/'
      });
  }

  function run($rootScope, $location) {
    $rootScope.location = $location;
  }

})();
