module.exports = (function() {
  'use strict';
  angular.module('game.console', []);
  require('./console.routes');
  require('./console.controller');
})();
