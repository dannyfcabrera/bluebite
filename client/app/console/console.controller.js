require('./console.module');
require('../services/data.service');

module.exports = (function() {
  'use strict';

  angular
      .module('game.console')
      .controller('ConsoleController', ConsoleController);

  ConsoleController.$inject = ['$route','$timeout','dataservice'];

  function ConsoleController($route,$timeout,dataservice) {
      var vm = this;

      vm.team = [];
      vm.questions = [];
      vm.count = 0;
      vm.correct = 0;
      vm.wrong = 0;
      vm.gameOver = false;
      vm.message = null;
      vm.reset = reset;
      vm.instructions = false;

      vm.selectedAnswer = selectedAnswer;

      activate();

      function activate() {
        return getTeam()
        .then(function() {
          getQuestions()
          .then(function() {
            //
          });
        });
      }

      function reset() {
        return $route.reload();
      }

      function selectedAnswer(question,answer) {
        checkAnswer(question,answer);
      }

      function checkAnswer(question,answer) {
        if (answer === vm.questions[question].id) {

          markAnswer(question,answer);
          updateScore(question,answer);
          $timeout(updateQuestion, 1000);
        } else {
          markAnswer(question,answer);
          updateScore(answer);
        }
        return;
      }


      function gameStatus() {
        if (vm.wrong == 3) {

          vm.message = "Oh no, looks like you lost. Please try again.";
          vm.gameOver = true;

        } else if (vm.correct == 15) {

          vm.message = "You Won!";
          vm.gameOver = true;

        } else {

        }
        return;
      }

      function updateScore(question,answer) {
        if (answer === vm.questions[question].id) {
          vm.correct = vm.correct + 1;
          $timeout(gameStatus, 200);
        } else {
          vm.wrong = vm.wrong + 1;
          $timeout(gameStatus, 200);
        }
        return;
      }

      function updateQuestion() {
        vm.count = vm.count + 1;
        vm.team.status = null;
        return vm.count;
      }

      function markAnswer(question,answer) {
        if (answer === vm.questions[question].id) {
          vm.team[answer].status = 'correct';
        } else {
          vm.team[answer].status = 'wrong';
        }
        $timeout(function () {
          vm.team[answer].status = null;
        }, 1000);

        return;
      }

      function getTeam() {
        return dataservice.getTeam()
          .then(function(data) {
              vm.team = data;
              return vm.team;
          });
      }

      function getQuestions() {
        return dataservice.getQuestions()
          .then(function(data) {
              vm.questions = data;
              return vm.questions;
          });
      }
  }

})();
