require('./console.module');

module.exports = (function() {
  'use strict';
  angular
      .module('game.console')
      .config(config);

  function config($routeProvider) {
    $routeProvider
      .when('/game', {
          templateUrl: 'app/console/console.html',
          controller: 'ConsoleController',
          controllerAs: 'vm'
      });
  }
})();
