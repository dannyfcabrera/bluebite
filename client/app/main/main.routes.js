require('./main.module');

module.exports = (function() {
  'use strict';
  angular
      .module('game.main')
      .config(config);

  function config($routeProvider) {
    $routeProvider
      .when('/', {
          templateUrl: 'app/main/main.html',
          controller: 'MainController',
          controllerAs: 'vm'
      });
  }
})();
