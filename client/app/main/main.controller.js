require('./main.module');

module.exports = (function() {
  'use strict';

  angular
      .module('game.main')
      .controller('MainController', MainController);

  MainController.$inject = ['$location'];

  function MainController($location) {

  }

})();
