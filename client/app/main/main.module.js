module.exports = (function() {
  'use strict';
  angular.module('game.main', []);
  require('./main.routes');
  require('./main.controller');
})();
