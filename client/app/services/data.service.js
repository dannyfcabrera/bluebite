require('../core/core.module');

module.exports = (function() {
  'use strict';

angular
    .module('game.core')
    .factory('dataservice', dataservice);

dataservice.$inject = ['$http'];

function dataservice($http) {
    return {
      getTeam: getTeam,
      getQuestions: getQuestions
    };

    function getTeam() {
        return $http.get('app/data.json')
            .then(getTeamComplete)
            .catch(getTeamFailed);

        function getTeamComplete(response) {
            return response.data.members;
        }

        function getTeamFailed(error) {
            console.log(error.data);
        }
    }

    function getQuestions() {
        return $http.get('app/data.json')
            .then(getQuestionsComplete)
            .catch(getQuestionsFailed);

        function getQuestionsComplete(response) {
            return response.data.questions;
        }

        function getQuestionsFailed(error) {
            console.log(error.data);
        }
    }
}

})();
